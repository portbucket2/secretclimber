﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is a manager which will give common functional supports. 
 * like Math Calculator.
 *  
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace com.alphapotato.utility
{
    public class Calculator : MonoBehaviour
    {
        // Generate random normalized direction
        public static Vector3 GetRandomDir()
        {
            return new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized;
        }

        // Generate random normalized direction
        public static Vector3 GetRandomDirXZ()
        {
            return new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)).normalized;
        }

        // Is Mouse over a UI Element? Used for ignoring World clicks through UI
        public bool IsPointerOverUI()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return true;
            }
            else
            {
                PointerEventData pe = new PointerEventData(EventSystem.current);
                pe.position = Input.mousePosition;
                List<RaycastResult> hits = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pe, hits);
                return hits.Count > 0;
            }
        }

        public Vector3 GetDirToMouse(Vector3 fromPosition)
        {
            Vector3 mouseWorldPosition = GetWorldTouchPosition();
            return (mouseWorldPosition - fromPosition).normalized;
        }

        public Vector3 GetVelocityForThisPoint(Vector3 startPosition, Vector3 endPosition, float angle)
        {
            Vector3 direction = endPosition - startPosition;
            float h = direction.y;
            direction.y = 0;
            float distance = direction.magnitude;
            float a = angle * Mathf.Deg2Rad;
            direction.y = distance * Mathf.Tan(a);
            distance += h / Mathf.Tan(a);

            // calculate velocity
            float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
            return velocity * direction.normalized;
        }

        public Vector3 GetForceForThisPoint(Vector3 startPosition, Vector3 endPosition, float angle, float mass)
        {
            return GetVelocityForThisPoint(startPosition, endPosition, angle) * mass / Time.fixedDeltaTime;
        }

        public Vector3 GetWorldTouchPosition()
        {
            Plane plane = new Plane(Vector3.up, 0);

            float distance;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out distance))
            {
                return ray.GetPoint(distance);
            }

            return Vector3.zero;
        }

        public Vector3 GetWorldTouchPosition(GameObject go, Vector3 lastTouchPoint)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1 << go.layer))
            {
                return hit.point;
            }

            return lastTouchPoint;
        }

        public float WrapAngle(float angle)
        {
            angle %= 360;
            return angle = angle > 180 ? angle - 360 : angle;
        }

        public float UnwrapAngle(float angle)
        {
            if (angle >= 0)
                return angle;

            angle = -angle % 360;

            return 360 - angle;
        }
    }
}
