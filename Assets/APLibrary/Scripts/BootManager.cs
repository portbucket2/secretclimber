using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootManager : APManager
{
    public override void Awake()
    {
        base.Awake();
        ChangeGameState(GameState.GAME_DATA_LOADED);
    }

    public override void GameDataLoad()
    {
        base.GameDataLoad();
        Invoke("LoadLastPlayedLevel", 2.5f);
    }

    void LoadLastPlayedLevel()
    {
        int lastPlayedLevelNo = GetModedLevelNumber();
        APTools.sceneManager.LoadLevel(lastPlayedLevelNo + 1);
    }

}
