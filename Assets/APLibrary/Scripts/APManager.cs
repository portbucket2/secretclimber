﻿using System;
using UnityEngine;
using com.alphapotato.utility;
using System.Collections;
using TMPro;

[DefaultExecutionOrder(ConstantManager.APManagerOrder)]
public class APManager : MonoBehaviour
{
    public static Action OnAddAPBehaviour;
    public static Action<APManager> OnSelfDistributionAction;
    public static event Action<GameState> OnChangeGameState;
    public static event Action OnNone;
    public static event Action OnGameDataLoad;
    public static event Action OnGameInitialize;
    public static event Action OnGameStart;
    public static event Action OnGameOver;
    public static event Action OnCompleteTask;
    public static event Action OnIncompleteTask;
    public static event Action OnPauseGamePlay;

    public GameplayData gameplayData = new GameplayData();
    public GameState gameState;
    public ParticleSystem gameOverEffect;
    public Animator gameStartingUI, gamePlayUI, gameSuccessUI, gameFaildUI, gameCustomUI;
    public TextMeshProUGUI levelText;
    public AudioClip gameWinAudioClip, gameLoseFailAudioClip;

    //[HideInInspector]
    public int totalGivenTask = 0, totalCompletedTask = 0, totalIncompleteTask = 0;
    [HideInInspector]
    public APTools APTools;
    public bool debugModeOn;

    GameState previousState;
    AudioSource runtimeAudioSource;

    private void OnEnable()
    {
        OnAddAPBehaviour += AddAPBehaviour;
    }

    private void OnDisable()
    {
        OnAddAPBehaviour -= AddAPBehaviour;
    }

    public virtual void Awake()
    {
        APTools = APTools.Instance;
        runtimeAudioSource = gameObject.AddComponent<AudioSource>();
        runtimeAudioSource.playOnAwake = false;
    }

    void AddAPBehaviour()
    {
        //Debug.LogError("New APBehaviour Added Successfully.");
        OnSelfDistributionAction?.Invoke(this);
    }

    public virtual void Start()
    {

    }

    public virtual void OnCompleteATask()
    {
        totalCompletedTask++;
        CompleteATask();

        if (totalCompletedTask.Equals(totalGivenTask))
        {
            gameplayData.isGameoverSuccess = true;
            gameplayData.gameEndTime = Time.time;

            ChangeGameState(GameState.GAME_PLAY_ENDED);
        }
    }

    public virtual void OnIncompleteATask()
    {
        totalIncompleteTask++;
        IncompleteATask();
    }

    public int GetModedLevelNumber()
    {
        return gameplayData.currentLevelNumber % ConstantManager.TOTAL_GAME_LEVELS;
    }

    public int GetLevelMultiplayer()
    {
        return gameplayData.currentLevelNumber / ConstantManager.TOTAL_GAME_LEVELS;
    }

    public virtual void NextLevel()
    {
        APTools.sceneManager.LoadNextLevel();
    }

    public virtual void ChangeGameState(GameState gameState)
    {
        StartCoroutine(ChangeState(gameState));
    }

    IEnumerator ChangeState(GameState gameState)
    {
        yield return null;
        switch (gameState)
        {
            case GameState.NONE:
                None();
                break;
            case GameState.GAME_DATA_LOADED:
                GameDataLoad();
                break;
            case GameState.GAME_INITIALIZED:
                GameInitialize();
                break;
            case GameState.GAME_PLAY_STARTED:
                GameStart();
                break;
            case GameState.GAME_PLAY_ENDED:
                GameOver();
                break;
            case GameState.GAMEPLAY_PAUSE:
                PauseGamePlay();
                break;
            case GameState.GAMEPLAY_UNPAUSE:
                gameState = UnPauseGame(gameState);
                break;
        }

        Debug.Log("Executing: " + gameState.ToString());
        this.gameState = gameState;
        OnChangeGameState?.Invoke(gameState);
    }

    public virtual void None()
    {
        OnNone?.Invoke();
    }

    public virtual void GameDataLoad()
    {
        gameplayData = APTools.savefileManager.LoadGameData();
        OnSelfDistributionAction?.Invoke(this);
        OnGameDataLoad?.Invoke();
    }

    public virtual void GameInitialize()
    {
        OnGameInitialize?.Invoke();
    }

    public virtual void GameStart()
    {
        gameplayData.gameStartTime = Time.time;
        levelText.text = "Level - " + (gameplayData.currentLevelNumber + 1);
        OnGameStart?.Invoke();
    }

    public virtual void GameOver()
    {
        gameplayData.gameEndTime = Time.time;
        OnGameOver?.Invoke();

        if (gameplayData.isGameoverSuccess)
        {
            gameplayData.currentLevelNumber++;
            gameSuccessUI.SetBool("Hide", false);
            gameOverEffect.Play();
            PlayThisSoundEffect(gameWinAudioClip);
        }
        else
        {
            gameFaildUI.SetBool("Hide", false);
            PlayThisSoundEffect(gameLoseFailAudioClip);
        }
        
        SaveGame();
    }

    public virtual void PlayThisSoundEffect(AudioClip audioClip)
    {
        runtimeAudioSource.clip = audioClip;
        runtimeAudioSource.Play();
    }

    public virtual void SaveGame()
    {
        APTools.savefileManager.SaveGameData(gameplayData);
    }

    public virtual void PauseGamePlay()
    {
        previousState = gameState;
    }

    public virtual GameState UnPauseGame(GameState gameState)
    {
        gameState = previousState;
        return gameState;
    }
    public virtual void ReloadLevel()
    {
        APTools.sceneManager.ReLoadLevel();
    }

    internal void CompleteATask()
    {
        OnCompleteTask?.Invoke();
    }

    internal void IncompleteATask()
    {
        OnIncompleteTask?.Invoke();
    }

    internal void DistributeAPManager()
    {
        OnSelfDistributionAction?.Invoke(this);
    }


}
