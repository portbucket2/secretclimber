﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollower : APBehaviour
{
    public Transform followTarget, lookTarget;

    [Header("Follow Properties")]
    public bool follow;
    [Range(0f, 1f)]
    public float followSmoothness;
    public Vector3 followOffset;

    [Header("Look Properties")]
    public bool lookAtTarget;
    [Range(0f, 1f)]
    public float lookSmoothness;
    public Vector3 lookOffset;

    Vector3 velocity;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    public virtual void FixedUpdate()
    {
        if (follow)
        {
            transform.position = Vector3.SmoothDamp(transform.position, followTarget.position + followOffset, ref velocity, followSmoothness);
        }

        if (lookAtTarget)
        {
            var targetRotation = Quaternion.LookRotation(lookTarget.transform.position + lookOffset - transform.position);
            transform.rotation = Quaternion.Slerp(targetRotation, transform.rotation, lookSmoothness);
        }
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
