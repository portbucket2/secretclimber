using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class ParabolaMove : MonoBehaviour
{
    public AnimationCurve curve = new AnimationCurve(new Keyframe(0f, 0f, 0f, 90f), new Keyframe(0.5f, 1f, 0f, 0f), new Keyframe(1f, 0f, 90f, 0f));
    public float timeToReach = 1, height = 3;
    public bool lookForwardOnMove, destroyOnComplete;
    public Transform endPoint;
    public bool initialized;

    Vector3 startPoint, lastPostion;
    float time, gap = 0.25f;
    Action action;
    // Awake is called before Start
    public void Awake()
    {
        lastPostion = transform.position;
        startPoint = transform.position;
    }

    public void Initialized(AnimationCurve curve, Transform endPoint, float height, float timeToReach, bool lookForwardOnMove, bool destroyOnComplete, Action action = null)
    {
        this.lookForwardOnMove = lookForwardOnMove;
        this.destroyOnComplete = destroyOnComplete;
        this.action = action;
        this.curve = curve;
        this.endPoint = endPoint;
        this.height = height;
        this.timeToReach = timeToReach;
        initialized = true;
    }

    void Update()
    {
        if (!initialized)
        {
            return;
        }

        time += Time.deltaTime;
        float lerpValue = Mathf.InverseLerp(0, timeToReach, time);
        Vector3 pos = Vector3.Lerp(startPoint, endPoint.position, lerpValue);
        pos.y += height * curve.Evaluate(lerpValue);
        transform.position = pos;

        if (lookForwardOnMove)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - lastPostion, Vector3.up);
        }

        if(lerpValue == 1)
        {
            action?.Invoke();
        }
        if (destroyOnComplete)
        {
            Destroy(this);
        }
    }

    private void LateUpdate()
    {
        lastPostion = transform.position;
    }


}
